#!/bin/bash
set -x

../../../bellerophon/build/bellerophon  -p ./ -r ../chimera/output/mutants/segmentation.c/2/loop_report.csv -t LoopAprx -P loopex.param 100
