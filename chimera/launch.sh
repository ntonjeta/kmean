#!/bin/bash
set -x

rm -rf output
../../../clang-chimera/build/clang-chimera -v -fun-op conf.csv -generate-mutants  ../src/segmentation.c -o output -- -I../src/ -I../src/include/ 
